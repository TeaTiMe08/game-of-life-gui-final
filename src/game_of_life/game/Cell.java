package game_of_life.game;

/**
 * Is an element of the matrix, that is handled in </GameMatrix>. The
 * column-position and row-position are also saved because in some
 * operations in </GameMatrix> it is needed for a better runtime.
 *
 * @version 28.05.2015
 */
public class Cell {

    private int numberOfLivingNeighbours;
    private boolean isAlive;
    private int colPosition;
    private int rowPosition;

    /**
     * Creates a new Cell and sets the params for identifying the cell.
     * @param isAlive       sets the cell alive or dead
     * @param colPosition   sets the column-position of the cell
     * @param rowPosition   sets the row-position of the cell
     */
    public Cell(boolean isAlive, int colPosition, int rowPosition) {
        this.isAlive = isAlive;
        this.colPosition = colPosition;
        this.rowPosition = rowPosition;
    }

    /**
     * Raises the recent number of living neighbours by one. Used at creating
     * the new generation in </GameMatrix>.
     */
    public void raiseNumberOfLvNb() {
        ++numberOfLivingNeighbours;
    }

    /**
     * Resets the recent number of living neighbours back to zero. Used at
     * creating the new generation in </GameMatrix>.
     */
    public void clearNumberOfLvNb() {
        numberOfLivingNeighbours = 0;
    }

    /**
     * @return the number of recently living neighbours.
     */
    public int getNumberOfLivingNeihgbours() {
        return numberOfLivingNeighbours;
    }

    /**
     * @return the actual column-position of the cell.
     */
    public int getColPosition() {
        return colPosition;
    }

    /**
     * @return the actual row-position of the cell.
     */
    public int getRowPosition() {
        return rowPosition;
    }

    /**
     * @return true if the cell is alive and false if the cell is dead
     */
    public boolean getIsAlive() {
        return isAlive;
    }

    /**
     * sets the cell alive or dead
     * @param newValue  the new value if the cell should live or die
     */
    public void setIsAlive(boolean newValue) {
        isAlive = newValue;
    }

}