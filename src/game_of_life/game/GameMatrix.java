package game_of_life.game;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;

/**
 * Represents the game_of_life.gui.game. Is the main operator that iterates over
 * the cell matrix.
 * Implements the given Interface </Grid>. Handles the following operations:
 * generating new matrix, reviving and killing cells, checking if a cell is
 * alive or dead, getting the size of the matrix, resizing the matrix,
 * returning a collection of the recently living cells, clearing of matrix,
 * generating the next generation, getting the number of generations and
 * printing the matrix.
 * The class </Shell> communicates between the system and the user, so the
 * GameMatrix receives commands from the shell.
 *
 * @version 28.05.2015
 */
public class GameMatrix implements Grid {

    /**
     * Cell[][] matrix : The matrix of cells
     * int generationCounter : counts the generations the matrix had
     * LinkedList<Cell> cellCol : a list of cells, representing the recently
     *                            living cells
     */
    private Cell[][] matrix;
    private int generationCounter;
    private LinkedHashSet<Cell> cellCol;

    /**
     * Generates a new game_of_life.gui.game and a new matrix with a given size
     * . Sets the generationCounter as zero and sets every cell in the matrix
     * as dead.
     * @param numberOfColumns gives the length of the matrix.
     * @param numberOfRows gives the height of the matrix.
     */
    public GameMatrix(int numberOfColumns, int numberOfRows) {
        matrix = new Cell[numberOfColumns][numberOfRows];
        generationCounter = 0;
        cellCol = new LinkedHashSet<>();

        // Generates all cells and sets them dead.
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                matrix[i][j] = new Cell(false, i, j);
            }
        }
    }

    /**
     * @param col the column-position for checking.
     * @param row the row-position for checking.
     * @return true if the cell is alive, false if the cell is dead.
     */
    public synchronized boolean isAlive(int col, int row) {
        return matrix[col][row].getIsAlive();
    }

    /**
     * @param col the column-position for setting.
     * @param row the row-position for setting.
     * @param alive if true sets the cell in the position alive otherwise false.
     */
    public synchronized void setAlive(int col, int row, boolean alive) {
        matrix[col][row].setIsAlive(alive);
        if (alive) {
            cellCol.add(matrix[col][row]);
        } else {
            cellCol.remove(matrix[col][row]);
        }
    }

    /**
     * Represents the matrix as a two-dimensional picture. "." illustrates a
     * dead cell and "X" a living cell.
     * @return the matrix represented as a String.
     */
    public String toString() {
        StringBuilder outString = new StringBuilder("");
        for (int i = 0; i < matrix[0].length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (isAlive(j, i)) {
                    outString.append("X");
                } else {
                    outString.append(".");
                }
            }
            if (i != matrix[0].length - 1) {
                outString.append("\n");
            }
        }
        return outString.toString();
    }

    /**
     * @return the length of the matrix.
     */
    public int getColumns() {
        return matrix.length;
    }

    /**
     * @return the height of the matrix.
     */
    public int getRows() {
        return matrix[0].length;
    }

    /**
     * Clears the matrix by killing all the living cells. Resets the
     * generationCounter to zero.
     */
    public void clear() {
        Cell[] cellArray = cellCol.toArray(new Cell[cellCol.size()]);
        for (int i = 0; i < cellCol.size(); i++) {
            matrix[cellArray[i].getColPosition()]
                    [cellArray[i].getRowPosition()].setIsAlive(false);
        }
        cellCol.clear();
        generationCounter = 0;
    }

    /**
     * @return number of generations.
     */
    public int getGenerations() {
        return generationCounter;
    }

    /**
     * @return a collection of the living cells.
     */
    public Collection<Cell> getPopulation() {
        return Collections.unmodifiableSet(cellCol);
    }

    /**
     * Resizes the matrix with the given params as new size. Overwrites the
     * living cells to the new matrix. The living cells that would be outside
     * of the matrix are not existent in the new one.
     * The generationCounter is untouched.
     * @param cols the new column-size/length for the matrix.
     * @param rows the new row-size/height for the matrix.
     */
    public void resize(int cols, int rows) {
        Cell[][] newMatrix = new Cell[cols][rows];
        LinkedHashSet<Cell> newCellCol = new LinkedHashSet<>();

        // Sets every cell in the new matrix as dead.
        for (int l = 0; l < cols; l++) {
            for (int m = 0; m < rows; m++) {
                newMatrix[l][m] = new Cell(false, l, m);
            }
        }

        // Overwrites the old living cells to the new matrix and add them to
        // a new list of living cells.
        Cell[] overwriteCellList = cellCol.toArray(new Cell[cellCol.size()]);
        for (int i = 0; i < overwriteCellList.length; i++) {
            Cell temporaryCell = overwriteCellList[i];
            int tempColPos = temporaryCell.getColPosition();
            int tempRowPos = temporaryCell.getRowPosition();
            if (tempColPos < newMatrix.length && tempRowPos
                    < newMatrix[0].length) {
                newMatrix[tempColPos][tempRowPos] = temporaryCell;
                newCellCol.add(temporaryCell);
            }
        }
        matrix = new Cell[cols][rows];
        matrix = newMatrix;
        cellCol = newCellCol;
    }

    /**
     * Generates the new generation of cells.  The number of living neighbours
     * of every cell is determined by iterating on the cell-collection and
     * raising the number of living neighbours in every surrounding cell by
     * one. These surrounding cells are together witch the checked cells
     * added to a check-array to calculate if the cell should keep living,
     * if it should die or if a dead cell should de enlivened. To avoid
     * having the same cell saved multiple times in the check-array, a
     * bool-matrix is generated in witch is saved if a cell that should be
     * added to the check-array was already added to the check-list. The
     * value in the bool-matrix for a already added cell is true, if the cell
     * yet was not added false.
     * The generationCounter is raised by one and the number os generations
     * is printed out.
     */
    public synchronized void next() {
        LinkedHashSet<Cell> checkList = new LinkedHashSet<>();
        Cell[] cellArray = cellCol.toArray(new Cell[cellCol.size()]);
        boolean[][] checkBoolArray = new boolean[getColumns()][getRows()];

        for (Cell recentCell : cellCol) {
            // Iterates over the surrounding cells.
            for (int l = -1; l < 2; l++) {
                for (int m = -1; m < 2; m++) {
                    int tempColPos = recentCell.getColPosition() + l;
                    int tempRowPos = recentCell.getRowPosition() + m;
                    // Catches the surrounding cell-positions that are
                    // stepping out of the matrix.
                    if (0 <= tempColPos && tempRowPos < matrix[0].length
                            && 0 <= tempRowPos
                            && tempColPos < matrix.length) {
                        if (m != 0 || l != 0) {
                            matrix[tempColPos][tempRowPos]
                                    .raiseNumberOfLvNb();
                        }
                        // Adds the recent cell that was not yet
                        // added to the check-array to it.
                        if (!checkBoolArray[tempColPos][tempRowPos]) {
                            checkList.add(
                                    matrix[tempColPos][tempRowPos]);
                            checkBoolArray[tempColPos][tempRowPos]
                                    = true;
                        }
                    }
                }
            }
        }
        Cell[] checkArray = checkList.toArray(new Cell[checkList.size()]);
        makeNewGeneration(checkArray);

        generationCounter++;
    }

    /**
     * Generates the new generation of Cells. Generates a new matrix and a
     * new cell-collection.
     * If a cell has zero or one neighbours the cell stays dead if it is dead
     * or dies if it is alive. If a cell has 2 neighbours it stays dead if it
     * is dead or stays alive if it is alive. If a cell has three neighbours
     * enlivens if it is dead or stays alive if it is alive. If a cell has
     * more than tree neighbours the cell dies if it is alive or stays dead
     * if it is dead already.
     * The new status of the cells is saved in the new matrix and the new
     * living cells are also saved in the new cell-collection.
     * The numberOfLivingNeighbours of every cell is reset to zero.
     * The new matrix and the new cell-collection are set as the recent
     * matrix and cell-collection.
     * @param checkArray gets an array of cells that must be checked if a
     *                   cell should live or die
     */
    private void makeNewGeneration(Cell[] checkArray) {
        Cell[][] newMatrix = this.matrix;
        LinkedHashSet<Cell> newCellCol = new LinkedHashSet<>();

        for (int i = 0; i < checkArray.length; i++) {
            Cell tempCell = checkArray[i];
            int tempColPos = tempCell.getColPosition();
            int tempRowPos = tempCell.getRowPosition();
            switch(tempCell.getNumberOfLivingNeihgbours()) {
                case 0 :
                    newMatrix[tempColPos][tempRowPos].setIsAlive(false);
                    break;
                case 1 :
                    newMatrix[tempColPos][tempRowPos].setIsAlive(false);
                    break;
                case 2 :
                    if (matrix[tempColPos][tempRowPos].getIsAlive()) {
                        newMatrix[tempColPos][tempRowPos].setIsAlive(true);
                        newCellCol.add(newMatrix[tempColPos][tempRowPos]);
                    }
                    break;
                case 3 :
                    newMatrix[tempColPos][tempRowPos].setIsAlive(true);
                    newCellCol.add(newMatrix[tempColPos][tempRowPos]);
                    break;
                // The case if the cell has more than three neighbours.
                default :
                    newMatrix[tempColPos][tempRowPos].setIsAlive(false);
            }
            newMatrix[tempColPos][tempRowPos].clearNumberOfLvNb();
        }
        matrix = newMatrix;
        cellCol = newCellCol;
    }
}
