package game_of_life.game;

import java.util.LinkedList;

/**
 * Is used by the class </Shell>. Loads and returns different footing
 * populations also called 'shapes'. The shapes are all static final.
 * The shapes are given as a LikedList importing cells.
 * Attention: The first cell always is a dummy-cell that contains the size of
 *            the specific shape. This is needed for calculating the
 *            position of the shape in the class </Shell>.
 *
 * @version 28.05.2015
 */
public abstract class Shape {

    private static LinkedList<Cell> cellList;

    /**
     * @return the shape "Block"
     */
    public static final LinkedList<Cell> block() {
        cellList = new LinkedList<Cell>();

        // First list-element gives size the specific shape.
        cellList.add(new Cell(false, 2, 2));

        cellList.add(new Cell(true, 0, 0));
        cellList.add(new Cell(true, 0, 1));
        cellList.add(new Cell(true, 1, 0));
        cellList.add(new Cell(true, 1, 1));
        return cellList;
    }

    /**
     * @return the shape "Boat"
     */
    public static final LinkedList<Cell> boat() {
        cellList = new LinkedList<Cell>();

        // First list-element gives size of specific shape.
        cellList.add(new Cell(false, 3, 3));

        cellList.add(new Cell(true, 0, 0));
        cellList.add(new Cell(true, 0, 1));
        cellList.add(new Cell(true, 1, 0));
        cellList.add(new Cell(true, 2, 1));
        cellList.add(new Cell(true, 1, 2));
        return cellList;
    }

    /**
     * @return the shape "Blinker"
     */
    public static final LinkedList<Cell> blinker() {
        cellList = new LinkedList<Cell>();

        // First list-element gives the size of the specific shape
        cellList.add(new Cell(false, 3, 1));

        cellList.add(new Cell(true, 0, 0));
        cellList.add(new Cell(true, 1, 0));
        cellList.add(new Cell(true, 2, 0));
        return cellList;
    }

    /**
     * @return the shape "Toad"
     */
    public static final LinkedList<Cell> toad() {
        cellList = new LinkedList<Cell>();

        // First list-element gives the size of the specific shape
        cellList.add(new Cell(false, 4, 2));

        cellList.add(new Cell(true, 1, 0));
        cellList.add(new Cell(true, 2, 0));
        cellList.add(new Cell(true, 3, 0));
        cellList.add(new Cell(true, 0, 1));
        cellList.add(new Cell(true, 1, 1));
        cellList.add(new Cell(true, 2, 1));
        return cellList;
    }

    /**
     * @return the shape "Glider"
     */
    public static final LinkedList<Cell> glider() {
        cellList = new LinkedList<Cell>();

        // First list-element gives the size of the specific shape
        cellList.add(new Cell(false, 3, 3));

        cellList.add(new Cell(true, 0, 0));
        cellList.add(new Cell(true, 1, 0));
        cellList.add(new Cell(true, 2, 0));
        cellList.add(new Cell(true, 0, 1));
        cellList.add(new Cell(true, 1, 2));
        return cellList;
    }

    /**
     * @return the shape "Spaceship"
     */
    public static final LinkedList<Cell> spaceship() {
        cellList = new LinkedList<Cell>();

        // First list-element gives the size of the specific shape
        cellList.add(new Cell(false, 5, 4));

        cellList.add(new Cell(true, 1, 0));
        cellList.add(new Cell(true, 4, 0));
        cellList.add(new Cell(true, 0, 1));
        cellList.add(new Cell(true, 0, 2));
        cellList.add(new Cell(true, 4, 2));
        cellList.add(new Cell(true, 0, 3));
        cellList.add(new Cell(true, 1, 3));
        cellList.add(new Cell(true, 2, 3));
        cellList.add(new Cell(true, 3, 3));
        return cellList;
    }

    /**
     * @return the shape "Pulsar"
     */
    public static final LinkedList<Cell> pulsar() {
        cellList = new LinkedList<Cell>();

        // First list-element gives the size of the specific shape
        cellList.add(new Cell(false, 13, 13));

        int[] row0 = {0, 0, 1, 1, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4, 5, 5, 5};
        int[] row1 = {2, 3, 3, 4, 0, 3, 5, 0, 1, 2, 4, 5, 1, 3, 5, 2, 3, 4};
        for (int i = 0; i < row0.length; i++) {
            cellList.add(new Cell(true, row0[i], row1[i]));
            cellList.add(new Cell(true, 12 - row0[i], row1[i]));
            cellList.add(new Cell(true, row0[i], 12 - row1[i]));
            cellList.add(new Cell(true, 12 - row0[i], 12 - row1[i]));
        }
        return cellList;
    }
}
