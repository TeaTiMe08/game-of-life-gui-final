package game_of_life.gui;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * A class that is created in the GoLPane. It extends Thread and so its
 * runnable. It automatically calculates the next generation at a specific
 * speed that can be changed at the runtime. The thread is stopped if the
 * runsNow is set false.
 */
public class GeneratingThread extends Thread {
    /**
     * A GoLPane that is given when the constructor is called so that the
     * thread can generate the next generation through the thread. Helpful
     * because when calling the method, the thread knows when to redraw the
     * pane.
     */
    private GoLPane pane;
    /**
     * An AtomicInteger that can be changed in the GoLPane class. If true the
     * thread keeps on running, when false the thread will stop. The value
     * can be changed while runtime.
     */
    private AtomicBoolean runsNow;
    /**
     * An AtomicInteger that represents the speed in witch the next
     * generation should be calculated. This speed can be changed in the
     * GoLPane while runtime.
     */
    private AtomicInteger recentSpeed;

    /**
     * The constructor for the GeneratingThread.
     * @param pane is the GoLPane that assigns itself by calling the
     *             constructor.
     * @param runsNow is set true for the thread to run.
     * @param recentSpeed is the given speed the thread should run.
     */
    public GeneratingThread(GoLPane pane, AtomicBoolean runsNow,
                            AtomicInteger recentSpeed) {
        this.pane = pane;
        this.runsNow = runsNow;
        this.recentSpeed = recentSpeed;
    }

    /**
     * By starting the thread the run() method is called. While the thread is
     * not stopped by changing the runsNow value to false, the thread will
     * keep on calculating the next generation at a certain time. When the
     * thread is suddenly interrupted by the system, the thread prints error.
     */
    @Override
    public void run() {
        while (runsNow.get()) {
            try {
                pane.next();
                sleep(recentSpeed.get());
            } catch (InterruptedException inter) {
                System.err.println("Error!: GeneratingThread suddenly "
                        + "stopped.");
            }
        }
    }
}