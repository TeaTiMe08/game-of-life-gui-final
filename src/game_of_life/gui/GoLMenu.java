package game_of_life.gui;

import game_of_life.game.Cell;
import game_of_life.game.Shape;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

/**
 * The GolMenu (game-of-life-menu) is a JPanel that is responsible for the
 * settings that can be made and controlling of the game. The user can select
 * a mode in witch way the mouse should treat the cells clicked on, a
 * specific shape that can be preloaded, a button to calculate the next
 * generation, a button that starts the automatic calculation, sliders for
 * the size of the cells and the speed od generating and an output for the
 * number of generations and the number of generations per second.
 */
public class GoLMenu extends JPanel implements IGeneration {
    /**
     * The button when pressed a new generation is calculated.
     */
    private JButton nextButton;
    /**
     * The JLabel for the output of the generations per second.
     */
    private JLabel genPerSecJLabel = makeGenerationPerSecText();
    /**
     * The JLabel for the output of the number of generations.
     */
    private JLabel generationJLabel = makeGenerationText();
    /**
     * The GoLPane that is notified when the user uses the menu.
     */
    private GoLPane pane;

    /**
     * When a new GoLMenu is created int the GUI the a new GoLPane is set and
     * the background is set black. Several components are created and added
     * to the JPanel.
     * @param inputPane the GoLPane given by the GUI to notify changes.
     */
    public GoLMenu(GoLPane inputPane) {
        pane = inputPane;
        setBackground(Color.black);
        // Sets the layout for the menu as FlowLayout so components can be
        // added flexibly.
        FlowLayout gr = new FlowLayout();
        gr.setHgap(5);
        gr.setVgap(5);
        setLayout(gr);
        // Adds all the components to the JPanel.
        add(makeMousePaintBoxPanel());
        add(makeShapeBoxPanel());
        nextButton = makeNextButton();
        add(nextButton);
        add(makePlayPauseButton());
        add(makeSizeSliderPanel());
        add(makeSpeedSliderPanel());
        add(makeGeneratingOutputPanel());
        //Finally sets the JPanel visible.
        setVisible(true);
    }

    /**
     * Creates a new JPanel that contains the output of the number of
     * generations and number of generations per second.
     * @return a JPanel containing the generation output.
     */
    private JPanel makeGeneratingOutputPanel() {
        JPanel generatingOutputPanel = new JPanel();
        generatingOutputPanel.setLayout(new FlowLayout());
        generatingOutputPanel.add(generationJLabel);
        generatingOutputPanel.add(genPerSecJLabel);
        generatingOutputPanel.setBackground(Color.blue);
        generatingOutputPanel.setBorder(new LineBorder(Color.WHITE, 1));
        return generatingOutputPanel;
    }

    /**
     * Creates a new JPanel that determines the mouse-paint-style.
     * @return a JPanel for setting the mouse-paint-style.
     */
    private JPanel makeMousePaintBoxPanel() {
        JPanel mousePaintBoxPanel = new JPanel();
        mousePaintBoxPanel.setLayout(new BorderLayout());
        mousePaintBoxPanel.add(makeMousePaintTitle(), BorderLayout.NORTH);
        mousePaintBoxPanel.add(makeMousePaintBox(), BorderLayout.CENTER);
        mousePaintBoxPanel.setBackground(Color.blue);
        mousePaintBoxPanel.setBorder(new LineBorder(Color.WHITE, 1));
        return mousePaintBoxPanel;
    }

    /**
     * Creates a new JPanel that can load a specific shape or clear the game.
     * @return a JPanel for setting a shape.
     */
    private JPanel makeShapeBoxPanel() {
        JPanel shapeBoxPanel = new JPanel();
        shapeBoxPanel.setLayout(new BorderLayout());
        shapeBoxPanel.add(makeShapeTitle(), BorderLayout.NORTH);
        shapeBoxPanel.add(makeShapeBox(), BorderLayout.CENTER);
        shapeBoxPanel.setBackground(Color.blue);
        shapeBoxPanel.setBorder(new LineBorder(Color.WHITE, 1));
        return shapeBoxPanel;
    }

    /**
     * Creates a new JPanel that can change the size of the cells.
     * @return a JPanel for setting the size of the cells.
     */
    private JPanel makeSizeSliderPanel() {
        JPanel sizeSliderPanel = new JPanel();
        sizeSliderPanel.setLayout(new BorderLayout());
        sizeSliderPanel.add(makeSizeTitle(), BorderLayout.NORTH);
        sizeSliderPanel.add(makeSizeSlider(), BorderLayout.CENTER);
        sizeSliderPanel.setBackground(Color.blue);
        sizeSliderPanel.setBorder(new LineBorder(Color.WHITE, 1));
        return sizeSliderPanel;
    }

    /**
     * Creates a new JPanel that can change the speed of the generating
     * thread.
     * @return a JPanel for setting the speed of generating.
     */
    private JPanel makeSpeedSliderPanel() {
        JPanel speedSliderPanel = new JPanel();
        speedSliderPanel.setLayout(new BorderLayout());
        speedSliderPanel.add(makeSpeedTitle(), BorderLayout.NORTH);
        speedSliderPanel.add(makeSpeedSlider(), BorderLayout.CENTER);
        speedSliderPanel.setBackground(Color.blue);
        speedSliderPanel.setBorder(new LineBorder(Color.WHITE, 1));
        return speedSliderPanel;
    }

    /**
     * Creates and decorated a JButton that if pressed creates a new generation
     * by using the GoLPane. It is deactivated while the GeneratingThread is
     * running.
     * @return a JButton for generating the next generation.
     */
    private JButton makeNextButton() {
        JButton next = new JButton();
        next.setToolTipText("Calculates the next generation.");
        next.setName("Next");
        next.setText("Next");
        next.setEnabled(true);
        Border border = new LineBorder(Color.WHITE, 1);
        next.setBorder(border);
        next.setBackground(Color.blue);
        next.setForeground(Color.white);
        next.setPreferredSize(new Dimension(45, 35));
        next.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pane.next();
            }
        });
        return next;
    }

    /**
     * Creates and decorates a JButton that if activated stats the thread for
     * automatically generating the next generation by using the GoLPane.
     * When pressed again the the thread stops.
     * @return a JButton for automatically generating new generations.
     */
    private JButton makePlayPauseButton() {
        JButton play = new JButton();
        play.setToolTipText("Automatically calculates the next generation.");
        play.setName("Play/Pause");
        play.setText("Play");
        play.setBackground(Color.blue);
        play.setForeground(Color.white);
        play.setPreferredSize(new Dimension(45, 35));
        Border border = new LineBorder(Color.WHITE, 1);
        play.setBorder(border);
        play.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (play.getText().equals("Play")) {
                    pane.runNext();
                    lockNextButton(true);
                    play.setText("Pause");
                } else {
                    pane.stopRunsNow();
                    genPerSecJLabel.setText("<html>Generations/sec<br>" + 0
                            + "</html>");
                    lockNextButton(false);
                    play.setText("Play");
                }
            }
        });
        return play;
    }

    /**
     * Creates a new JComboBox through witch the user can select witch
     * mouse-paint-style he wants to select. If "Enliven" is selected the
     * mouse is just able to enlive new cells. If "Kill" is selected the mouse
     * is just able to kill living cells. If "Enliven/Kill" is selected the
     * mouse enlives dead cells and kills living cells at the same time.
     * @return a JComboBox for selecting the mouse-paint-style.
     */
    private JComboBox makeMousePaintBox() {
        String[] names = {"Enliven/Kill", "Enliven", "Kill"};
        JComboBox<String> comboBox = new JComboBox<>(names);
        comboBox.setToolTipText("Sets the style how you want to paint the "
                + "cells");
        comboBox.setBackground(Color.blue);
        comboBox.setForeground(Color.white);
        comboBox.addActionListener(new ActionListener() {
            /**
             * Changes the mouse-paint-style.
             * @param e the ActionEvent when a item in the JComboBox is
             *          selected.
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                String item = comboBox.getSelectedItem().toString();
                switch (item) {
                    case "Enliven/Kill":
                        pane.setPaintStyle(GoLPane.PaintStyle.ENLIVENKILL);
                        break;
                    case "Enliven":
                        pane.setPaintStyle(GoLPane.PaintStyle.ENLIVEN);
                        break;
                    case "Kill":
                        pane.setPaintStyle(GoLPane.PaintStyle.KILL);
                        break;
                    default:
                        break;
                }
            }
        });
        comboBox.setVisible(true);
        return comboBox;
    }

    /**
     * Creates a JComboBox through witch a specific shape can be loaded. When
     * the shape "No Shape" was loaded, the game clears. When a new shape was
     * loaded the generationCounter is resetted to zero.
     * @return a JComboBox for loading a shape.
     */
    private JComboBox makeShapeBox() {
        String[] names = {"No Shape", "Block", "Boat", "Blinker", "Toad"
                , "Glider", "Spaceship", "Pulsar"};
        JComboBox<String> comboBox = new JComboBox<>(names);
        comboBox.setToolTipText("Loads a specific shape.");
        comboBox.setBackground(Color.blue);
        comboBox.setForeground(Color.white);
        comboBox.addActionListener(new ActionListener() {
            /**
             * When a shape was selected it is validated and loaded by using
             * assigning the cell-list to the GoLPane.
             * @param e the ActionEvent when a shape is selected.
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                String item = (String) comboBox.getSelectedItem();
                validateShape(item);
            }

            private void validateShape(String item) {
                LinkedList<Cell> inputList;
                switch (item) {
                    case "Block":
                        inputList = Shape.block();
                        break;
                    case "Boat":
                        inputList = Shape.boat();
                        break;
                    case "Blinker":
                        inputList = Shape.blinker();
                        break;
                    case "Toad":
                        inputList = Shape.toad();
                        break;
                    case "Glider":
                        inputList = Shape.glider();
                        break;
                    case "Spaceship":
                        inputList = Shape.spaceship();
                        break;
                    case "Pulsar":
                        inputList = Shape.pulsar();
                        break;
                    default:
                        inputList = null;
                }
                pane.awakeShape(inputList);
            }
        });
        comboBox.setVisible(true);

        return comboBox;
    }

    /**
     * Creates a new JSlider that is used for changing the size of the cells.
     * @return a new JSlider for setting the size of the cells.
     */
    private JSlider makeSizeSlider() {
        JSlider sizeSl = new JSlider(5, 100);

        sizeSl.setToolTipText("Resets the size of the cells.");
        // The size can just be changed by a space of 5 pixel.
        sizeSl.setMajorTickSpacing(5);
        sizeSl.setSnapToTicks(true);
        sizeSl.setValue(25);
        sizeSl.setForeground(Color.white);
        sizeSl.setBackground(Color.blue);
        sizeSl.addChangeListener(new ChangeListener() {
            /**
             * The slider changed the size of the cells. While the
             * GeneratingThread is not running the size of the cells can be
             * changed dynamically, if not, the size of the of the cells is
             * just changing when the mouse releases the slider for
             * preventing slow runtime caused by the synchronized access to
             * the game.
             * @param e the ActionEvent when the JSlider is moved or pressed.
             */
            @Override
            public void stateChanged(ChangeEvent e) {
                if (!pane.getRunsNow()) {
                    pane.setRecentSize(sizeSl.getValue());
                    pane.setNewGameSize();
                } else {
                    if (!sizeSl.getValueIsAdjusting()) {
                        pane.setRecentSize(sizeSl.getValue());
                        pane.setNewGameSize();
                    }
                }
            }
        });
        return sizeSl;
    }

    /**
     * Creates a new JSlider that can change the speed that the new
     * generations are calculated in the GeneratingThread.
     * @return a JSlider for setting the generating-speed.
     */
    private JSlider makeSpeedSlider() {
        JSlider speedSl = new JSlider(0, 100);
        speedSl.setToolTipText("Resets the speed the new generation is "
                + "calculated.");
        speedSl.setInverted(false);
        speedSl.setValue(50);
        speedSl.setForeground(Color.white);
        speedSl.setBackground(Color.blue);
        speedSl.addChangeListener(new ChangeListener() {
            /**
             * By moving the slider the speed is changed in a steep parabola
             * so that a it is not a linear growth and it the user feels
             * better when using the slider.
             * @param e the ActionEvent when the slider is moved
             */
            @Override
            public void stateChanged(ChangeEvent e) {
                pane.setRecentSpeed(
                        (int) (0.1 * Math.pow(speedSl.getValue(), 2)));
            }
        });
        return speedSl;
    }

    /**
     * Creates a new JLabel is an output for the generations per second.
     * @return a JLabel for chowing the generations per second.
     */
    private JLabel makeGenerationPerSecText() {
        genPerSecJLabel = new JLabel();
        genPerSecJLabel.setToolTipText("The number of jet calculated "
                + "generations.");
        genPerSecJLabel.setText("<html>Generations/sec<br>" + 0 + "</html>");
        genPerSecJLabel.setBackground(Color.blue);
        genPerSecJLabel.setForeground(Color.white);
        genPerSecJLabel.setVisible(true);
        return genPerSecJLabel;
    }

    /**
     * Creates a JLabel for the title of the mouse-paint-style-box.
     * @return a JLabel with the heading "Mouse-Paint-Style".
     */
    private JLabel makeMousePaintTitle() {
        JLabel speed = new JLabel();
        speed.setText("Mouse-Paint-Style");
        speed.setHorizontalAlignment(SwingConstants.CENTER);
        speed.setForeground(Color.white);
        speed.setVisible(true);
        return speed;
    }

    /**
     * Creates a JLabel for the title of the shape-box.
     * @return a JLabel with the heading "Shape".
     */
    private JLabel makeShapeTitle() {
        JLabel speed = new JLabel();
        speed.setText("Shape");
        speed.setHorizontalAlignment(SwingConstants.CENTER);
        speed.setForeground(Color.white);
        speed.setVisible(true);
        return speed;
    }

    /**
     * Creates a JLabel for the title of the speed-box.
     * @return a JLabel with the heading "Speed".
     */
    private JLabel makeSpeedTitle() {
        JLabel speed = new JLabel();
        speed.setText("Speed");
        speed.setHorizontalAlignment(SwingConstants.CENTER);
        speed.setForeground(Color.white);
        speed.setVisible(true);
        return speed;
    }

    /**
     * Creates a JLabel for the title of the size-box.
     * @return a JLabel with the heading "Size".
     */
    private JLabel makeSizeTitle() {
        JLabel size = new JLabel();

        size.setText("Size");
        size.setHorizontalAlignment(SwingConstants.CENTER);
        size.setForeground(Color.white);
        size.setVisible(true);

        return size;
    }

    /**
     * Creates a JLabel for the text of the number of generations.
     * @return a JLabel for the output number of generations.
     */
    private JLabel makeGenerationText() {
        JLabel generationJLabel = new JLabel();
        generationJLabel.setToolTipText("The number of jet calculated "
                + "generations.");
        generationJLabel.setText("" + 0);
        generationJLabel.setBackground(Color.blue);
        generationJLabel.setForeground(Color.white);
        generationJLabel.setVisible(true);
        return generationJLabel;
    }

    /**
     * Locks the "Next"-JButton while the GeneratingThread is running.
     * @param lock true if the lock should be activated, false to unlock the
     *             button.
     */
    public void lockNextButton(boolean lock) {
        if (lock) {
            nextButton.setEnabled(false);
        } else {
            nextButton.setEnabled(true);
        }
    }

    /**
     * The text of the output for the recent generation is updated with the
     * new generation.
     * @param generation the new number of generation.
     */
    @Override
    public void nextGeneration(int generation) {
        generationJLabel.setText("<html>Generation<br>" + generation
                + "</html>");
    }

    /**
     * The text of the output for the recent generations per second is
     * updated with the new number.
     * @param generationPerSecond the new number of generations per second.
     */
    @Override
    public void nextGenerationPerSecond(int generationPerSecond) {
        genPerSecJLabel.setText("<html>Generations/sec<br>"
                + generationPerSecond + "</html>");
    }
}
