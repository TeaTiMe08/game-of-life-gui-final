package game_of_life.gui;

import game_of_life.game.Grid;
import game_of_life.game.GameMatrix;
import game_of_life.game.Cell;

import java.awt.Point;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Collection;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * The GoLPane (game-of-life-pane) is the main controller of the GUI. It
 * controls and updated the GoLView and the Grid. It also updated the
 * generation and generation per second in the GolMenu. The GoLPane listens
 * and reacts to the windowSize and communicates between the menu and the game.
 */
public class GoLPane implements IGetGeneration {
    /**
     * The "Game of Life" game itself.
     */
    private Grid game;
    /**
     * When something in the game changed the view will be updated.
     */
    private GoLView view;
    /**
     * Through the interface the GoLPane can update the output of the
     * generation and generation per second in the GoLMenu.
     */
    private IGeneration generationMenu;
    /**
     * The recent speed that is updated in the GoLMenu. It updates the speed
     * also while the GenerationThread is running.
     */
    private AtomicInteger recentSpeed;
    /**
     * True if the GenerationThread and the GpSThread are recently running,
     * otherwise false. The value can be set by the GoLMenu.
     */
    private AtomicBoolean runsNow;
    /**
     * The definition of the mouse-paint-style. When it is set ENLIVENKILL the
     * dead cells are enliven and living cells dead. When it is set
     * ENLIVEN all clicked on cells are set alive. When it is set KILL it
     * just kills all living cells clicked on.
     */
    public enum PaintStyle {
        /**
         * Elivens dead cells, kills dead cells with the mouse.
         */
        ENLIVENKILL,
        /**
         * Enlivens all cells with the mouse.
         */
        ENLIVEN,
        /**
         * Kills all cells with the mouse.
         */
        KILL
    }
    /**
     * The recent state of the mouse-paint-style.
     */
    private PaintStyle paintStyle;
    /**
     * Always saved the recent number of generations. Needed for the
     * GpSThread to request the number of generations to calculate the
     * generations per second.
     */
    private AtomicInteger actualGeneration;

    /**
     * When a new GoLPane is created a new Game is created with the columns
     * with the number of width lines and the rows with the number of height
     * lines. MouseListener and a WindowSizeListener are added to the GoLView.
     * @param view the given GoLView for the GoLPane to update the view.
     */
    public GoLPane(GoLView view) {
        this.view = view;
        game = new GameMatrix(view.getNumberWidthLines(),
                view.getNumberHeightLines());
        runsNow = new AtomicBoolean(false);
        recentSpeed = new AtomicInteger(250);
        actualGeneration = new AtomicInteger(0);
        paintStyle = PaintStyle.ENLIVENKILL;
        // Adds the MouseListener to the GolView.
        view.addMouseListener(mouseClicker());
        view.addMouseMotionListener(mousePainter());
        // Adds the window-size-listener to the GoLView.
        view.addComponentListener(listenWindowSize());
    }

    /**
     * When the frame is resized by the user, the GoLPane reacts by calling
     * the setNewWindowSize() method.
     * @return a ComponentListener that listens on the size of the pane.
     */
    private ComponentListener listenWindowSize() {
        ComponentListener cl = new ComponentListener() {
            /**
             * When the window is resized the setNewSize() method is called.
             * @param e the ComponentEvent when the windowSize is changed.
             */
            @Override
            public void componentResized(ComponentEvent e) {
                setNewGameSize();
            }
            /**
             * Does not affect the program.
             * @param e the ComponentEvent when the window is moved
             */
            @Override
            public void componentMoved(ComponentEvent e) { }
            /**
             * Does not affect the program.
             * @param e the ComponentEvent when the window is shown.
             */
            @Override
            public void componentShown(ComponentEvent e) { }
            /**
             * Does not affect the program.
             * @param e the ComponentEvent when the window is hidden.
             */
            @Override
            public void componentHidden(ComponentEvent e) { }
        };
        return cl;
    }

    /**
     * The MouseListener reacts on clicking on the JPanel of the GoLView. It
     * reacts on the game by considering the mouse-paint-style.
     * @return a new MouseListener that reacts on the game by clicking.
     */
    private MouseListener mouseClicker() {
        MouseListener ml = new MouseListener() {
            /**
             * When the mouse is clicked the position of the mouse over
             * the grid is calculated and the cell the mouse overlaps is set
             * -depending on the mouse-paint-style - as dead or alive. After
             * that the generation is updated ant the GoLView is repainted.
             * @param e the MouseEvent when the mouse is clicked.
             */
            @Override
            public void mouseClicked(MouseEvent e) {
                int size = view.getRecentSize();
                // Gets the actual Point of the mouse and its coordinates.
                Point point = e.getPoint();
                int x = (int) point.getX();
                int y = (int) point.getY();
                // Calculates over witch cell the mouse is located.
                int realX = x / size;
                int realY = y / size;
                if (game.getColumns() > realX  && game.getRows() > realY
                        && realX > -1 && realY > -1) {
                    switch (paintStyle) {
                        case ENLIVENKILL:
                            if (game.isAlive(realX, realY)) {
                                game.setAlive(realX, realY, false);
                            } else {
                                game.setAlive(realX, realY, true);
                            }
                            break;
                        case ENLIVEN:
                            game.setAlive(realX, realY, true);
                            break;
                        case KILL:
                            game.setAlive(realX, realY, false);
                            break;
                        default:
                            break;
                    }
                    updateGeneration();
                    view.repaint();
                }
            }
            /**
             * Does not affect the program.
             * @param e the MouseEvent when the mouse is pressed.
             */
            @Override
            public void mousePressed(MouseEvent e) { }
            /**
             * Does not affect the program.
             * @param e the MouseEvent when the mouse is released.
             */
            @Override
            public void mouseReleased(MouseEvent e) { }
            /**
             * Does not affect the program.
             * @param e the MouseEvent when the mouse is entered.
             */
            @Override
            public void mouseEntered(MouseEvent e) { }
            /**
             * Does not affect the program.
             * @param e the MouseEvent when the mouse exists.
             */
            @Override
            public void mouseExited(MouseEvent e) { }
        };
        return ml;
    }

    /**
     * The method is executed when the mouse is dragged and moving over the
     * GoLView. The position of the mouse over the grid is calculated and the
     * cells at the mouse position are set -depending on the paintStyle- deat
     * or alive.
     * @return a MouseMotionListener for reacting on dragging the mouse.
     */
    private MouseMotionListener mousePainter() {
        // The last point changed is stored so that the the method reacts
        // only when a new point is reached.
        Point lastPoint = new Point(-1, -1);
        MouseMotionListener ml = new MouseMotionListener() {
            /**
             * While the mouse is dragged the position of the mouse moved over
             * the grid is calculated and the cells the mouse reached are set
             * -depending on the mouse-paint-style - as dead or alive. After
             * that the generation is updated and the GoLView is repainted.
             * @param e the MouseEvent when the mouse is dragged.
             */
            @Override
            public void mouseDragged(MouseEvent e) {
                int size = view.getRecentSize();
                // Gets the actual Point of the mouse and its coordinates.
                Point point = e.getPoint();
                int x = (int) point.getX();
                int y = (int) point.getY();
                // Calculates over witch cell the mouse is located.
                int realX = x / size;
                int realY = y / size;
                if (game.getColumns() > realX  && game.getRows() > realY
                        && realX > -1 && realY > -1) {
                    // First changes if the mouse reaches a new cell.
                    if (!lastPoint.getLocation().equals(
                            new Point(realX, realY))) {
                        switch (paintStyle) {
                            case ENLIVENKILL:
                                if (game.isAlive(realX, realY)) {
                                    game.setAlive(realX, realY, false);
                                } else {
                                    game.setAlive(realX, realY, true);
                                }
                                break;
                            case ENLIVEN:
                                game.setAlive(realX, realY, true);
                                break;
                            case KILL:
                                game.setAlive(realX, realY, false);
                                break;
                            default:
                                break;
                        }
                        // The last Point changed is updated.
                        lastPoint.setLocation(new Point(realX, realY));
                        updateGeneration();
                        view.repaint();
                    }
                }
            }
            /**
             * Does not affect the program.
             * @param e the MouseEvent when the mouse is moved.
             */
            @Override
            public void mouseMoved(MouseEvent e) { }
        };
        return ml;
    }

    /**
     * Orders the Grid to calculate the next generation, updated the
     * generation and orders the GoLView to repaint. The method is
     * synchronized because of the access to the population of the game.
     */
    public synchronized void next() {
        game.next();
        updateGeneration();
        view.repaint();
    }

    /**
     * By calling the method the game automatically created the next
     * generation at a speed. It sets runNow as true, creates a new
     * GeneratingThread and a new GpSThread. After that both threads are
     * started. The threads are stopped by setting the runsNow value false.
     * It has synchronized access for protecting the Grid.
     */
    public synchronized void runNext() {
        runsNow.set(true);
        GeneratingThread thread0 = new GeneratingThread(this, runsNow,
                recentSpeed);
        GpSThread thread1 = new GpSThread(actualGeneration, generationMenu,
                runsNow);
        thread0.start();
        thread1.start();
    }

    /**
     * The method is called by the GoLMenu. When a shape is selected, the
     * game is cleared and if the given list of cells is not empty, the cells
     * in the list are set alive in the Grid.
     * @param inputList a list of cells that should be enliven.
     */
    public synchronized void awakeShape(LinkedList<Cell> inputList) {
        if (inputList != null) {
            game.clear();
            // The element on the front is removed because its a dead cell
            // that contains the size of the shape.
            Cell shapeSize = inputList.remove(0);
            int shapeSizeX = shapeSize.getColPosition();
            int shapeSizeY = shapeSize.getRowPosition();
            // If the selected shape fits in the JPanel.
            if (game.getColumns() >= shapeSizeX && game.getRows()
                    >= shapeSizeY) {
                // The position of the cells is calculated and very cell in
                // the list is set alive.
                for (Cell recentCell : inputList) {
                    int inputX = ((game.getColumns() - shapeSizeX) / 2)
                            + recentCell.getColPosition();
                    int inputY = ((game.getRows() - shapeSizeY) / 2)
                            + recentCell.getRowPosition();
                    game.setAlive(inputX, inputY, true);
                }
            }
        } else {
            game.clear();
        }
        updateGeneration();
        view.repaint();
    }

    /**
     * Every time the population of the game or the size  has changed the
     * method is executed for updating the recent number of generations and
     * assigning the recent population to the GoLView. The access is
     * synchronized because it also accesses the Population of the game.
     */
    public synchronized void updateGeneration() {
        int recentGeneration = game.getGenerations();
        generationMenu.nextGeneration(game.getGenerations());
        actualGeneration.set(recentGeneration);
    }

    /**
     * Every time the size of the cells or the size of the matrix has changed
     * the game is resized with the new number of vertical lines as rows and
     * with the new number if horizontal lines as columns. After resizing the
     * generation is updated and the GoLView repainted.
     */
    public synchronized void setNewGameSize() {
        game.resize(view.getNumberWidthLines(), view.getNumberHeightLines());
        updateGeneration();
        view.repaint();
    }

    /**
     * The method is called by the speedSlider in the GoLMenu. It resets the
     * speed the generations are calculated.
     * @param recentSpeed the new value for the recentSpeed.
     */
    public void setRecentSpeed(int recentSpeed) {
        this.recentSpeed.set(recentSpeed);
    }

    /**
     * The method is called by the sizeSlider in the GoLMenu. It resets the
     * size of the cells.
     * @param recentSize the new value for the size of the cells.
     */
    public void setRecentSize(int recentSize) {
        view.setRecentSize(recentSize);
    }

    /**
     * Sets the value for runs now false and stops the running GeneratingThread.
     */
    public void stopRunsNow() {
        this.runsNow.set(false);
    }

    /**
     * Sets the new mouse-paint-style. Used in the GoLMenu.
     * @param paintStyle the param for the new mouse-paint-style.
     */
    public void setPaintStyle(PaintStyle paintStyle) {
        this.paintStyle = paintStyle;
    }

    /**
     * Returns if the GeneratingThread is running or not.
     * @return true, if the threads are running, otherwise false.
     */
    public boolean getRunsNow() {
        return runsNow.get();
    }

    /**
     * Sets the menu as an interface so that the GoLPane can affect the
     * output number of generations and number of generations per second.
     * @param menu the given GoLMenu from the GUI.
     */
    public void setMenu(IGeneration menu) {
        generationMenu = menu;
        updateGeneration();
    }

    /**
     * Returns the actual population as a collection.
     * @return the recent population of the Grid.
     */
    public Collection<Cell> getCellCollection() {
        return game.getPopulation();
    }
}
