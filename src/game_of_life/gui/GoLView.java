package game_of_life.gui;

import game_of_life.game.Cell;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Collection;

/**
 * The GoLView (game-of-life-view) is a extended JPanel that illustrated the
 * game. It prints a grid that is the size of the recent game and prints the
 * recently living cells. The size of the Pane does not effect on the size of
 * the cells but on the size of the matrix, so that the amount of cells is
 * changing.
 */
public class GoLView extends JPanel {
    /**
     * The recent size of the cells. It can be changed in the GoLMenu by the
     * size-slider. Changing the window size does not affect the size of the
     * cells.
     */
    private int recentSize = 25;
    /**
     * The pane that is saved by using the interface IGetGenerations. It is
     * used for getting the actual population of cells, so it can be painted
     * on the JPanel.
     */
    private IGetGeneration pane;

    /**
     * A new GoLView is created and the background of the JPanel is set black.
     */
    public GoLView() {
        setBackground(Color.black);
    }

    /**
     * The recent size is updated, used in the GoLPane where the GoLMenu
     * notifies when the slider is used.
     * @param recentSize the new size of the cells.
     */
    public void setRecentSize(int recentSize) {
        this.recentSize = recentSize;
    }

    /**
     * Get the recent size of the cells.
     * @return the size of the cells.
     */
    public int getRecentSize() {
        return recentSize;
    }

    /**
     * The number of vertical lines of the grid by dividing the width of the
     * JPanel by the recent size of the cells.
     * @return the number of width lines.
     */
    public int getNumberWidthLines() {
        return getWidth() / recentSize;
    }

    /**
     * The number of horizontal lines os the grid by dividing the height of
     * the JPanel by the recent size of the cells.
     * @return the number of height lines.
     */
    public int getNumberHeightLines() {
        return getHeight() / recentSize;
    }

    /**
     * Paints the grid and the recently living cells on the JPanel. This method
     * is called by the GoLPane. Certain values are calculated before calling
     * the methods for painting the grid and the recent cells because of the
     * constant change of these values. The width and height as well as the
     * number of horizontal and vertical lines and the size are assigned to
     * the submethods.
     * @param g the Graphics through that the JPanel is painted.
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        // The values that are needed for painting the grid and the cells.
        int width = getWidth();
        int height = getHeight();
        int size = recentSize;
        int numberWidthLines = height / size;
        int numberHeightLines = width / size;
        // The methods for painting the grid and the cells. The specific
        // values are assigned.
        paintAliveCells(g, size);
        paintGrid(g2, size, numberWidthLines, numberHeightLines, width, height);
    }

    /**
     * Paints the recently living cells on the JPanel. The color of the cells
     * is lightGrey. For every cell in the cellCollection given by the
     * GoLPane a rectangle is painted at the position of the cell. This
     * method is only called by the paintComponent() method.
     * @param g the Graphics through witch the cells are painted on the JPanel.
     * @param size the recent size of the cells.
     */
    @SuppressWarnings("unchecked")
    private void paintAliveCells(Graphics g, int size) {
        for (Cell inputCell : (Collection<Cell>) pane.getCellCollection()) {
            // The positions for painting the cells is calculated by
            // multiplying the position of the cell by the recent size of the
            // cell.
            int x = inputCell.getColPosition() * size;
            int y = inputCell.getRowPosition() * size;
            g.setColor(Color.lightGray);
            g.fillRect(x, y, size, size);
        }
    }

    /**
     * Paints the grid on the JPanel. The color of the lines is set gray. This
     * method is only called by the paintComponent() method.
     * @param g2 the 2D graphics through witch the grid must be painted.
     * @param size the recent size of the cells.
     * @param numberWidthLines the number of width lines.
     * @param numberHeightLines the number of height lines.
     * @param maxWidth the recent width of the JPanel.
     * @param maxHeight the recent height of the JPanel.
     */
    private void paintGrid(Graphics2D g2, int size, int numberWidthLines,
                           int numberHeightLines, int maxWidth, int maxHeight) {
        g2.setColor(Color.gray);
        // Paints the horizontal lines of the grid.
        for (int i = 1; i < numberWidthLines + 1; i++) {
            int layer = i * size;
            g2.drawLine(0, layer, maxWidth, layer);
        }
        // Paints the vertical lines of the grid.
        for (int j = 1; j < numberHeightLines + 1; j++) {
            int layer = j * size;
            g2.drawLine(layer, maxHeight, layer, 0);
        }
    }

    /**
     * Sets the pane as an IGetGeneration called by the GUI.
     * @param pane the pane that should be handled.
     */
    public void setPane(IGetGeneration pane) {
        this.pane = pane;
    }
}
