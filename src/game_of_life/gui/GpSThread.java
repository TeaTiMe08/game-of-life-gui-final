package game_of_life.gui;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * The class GpSThread (generations-per-second-thread) is a thread created in
 * the GoLPane, simultaneously to the GeneratingThread. It measures the
 * generations calculated per second and shows the result in the GoLMenu.
 * When the thread is started it measures the generations per second every
 * 1000ms. The thread stops if the runsNow variable is set false.
 */
public class GpSThread extends Thread {
    /**
     * The menu that the GoLPane has is assigned by calling the constructor.
     * It uses the same interface as the GoLPane. It just can change the
     * generations and the generations per second in the GoLMenu.
     */
    private IGeneration menu;
    /**
     * It is an AtomicInteger initialized in the GoLPane. It has a reference
     * to the recent number of generations. Every time the number of
     * generations in the game has changed it is updated in the GoLPane. It
     * is needed for reading and calculating the generations per second.
     */
    private AtomicInteger recentGeneration;
    /**
     * An int value of generations that have passed. Its updated when the
     * calculations of the generations per second has finished.
     */
    private int oldGeneration;
    /**
     * An AtomicInteger that can be changed in the GoLPane class. If true the
     * thread keeps on running, when false the thread will stop. The value
     * can be changed while runtime.
     */
    private AtomicBoolean runsNow;

    /**
     * The constructor of the GpSThread.
     * @param actualGeneration the AtomicInteger assigned from the GoLPanes
     *                         actual generation AtomicInteger.
     * @param menu the GoLMenu assigned, stored as an IGeneration interface.
     * @param runsNow is set true when the thread should run.
     */
    public GpSThread(AtomicInteger actualGeneration, IGeneration menu,
                     AtomicBoolean runsNow) {
        recentGeneration = actualGeneration;
        this.menu = menu;
        this.runsNow = runsNow;
        oldGeneration = 0;
    }

    /**
     * While the thread is running, the generations per second are calculated
     * every second. While the thread is not stopped by changing the
     * runsNow variable to false, the thread reads the value of the
     * recentGenerations and subtracts the oldGeneration value. The
     * difference is remitted to the GoLMenu through the interface and the
     * oldGeneration will be updated. When the thread is suddenly interrupted
     * by the system it prints error.
     */
    @Override
    public void run() {
        try {
            while (runsNow.get()) {
                int nowGen = recentGeneration.get();
                int recentChange = nowGen - oldGeneration;
                menu.nextGenerationPerSecond(recentChange);
                oldGeneration = nowGen;
                sleep(1000);
            }
        } catch (InterruptedException e) {
            System.err.println("Error!: GpSThread suddenly stopped.");
        }
    }
}
