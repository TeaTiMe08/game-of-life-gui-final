package game_of_life.gui;

/**
 * The interface is needed for the GoLMenu to get the actual number of
 * generation and the actual number of generations per second. Thereby these
 * number can be shown to the User. The GoLMenu implements the interface so
 * the GoLPane can update the actual number.
 */
public interface IGeneration {
    /**
     * Updates the recent number of generations in the GoLMenu.
     * @param generation sets the new recent number of generations from the
     *                   Grid.
     */
    void nextGeneration(int generation);

    /**
     * Updates the recent number of generations in the GoLMenu.
     * @param generationPerSecond sets the new recent number of generations
     *                            from the Grid.
     */
    void nextGenerationPerSecond(int generationPerSecond);
}
