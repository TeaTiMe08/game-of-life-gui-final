package game_of_life.gui;

import java.util.Collection;

/**
 * The interface IGetGeneration is needed for the GoLView to know where the
 * Cells are that must be painted.
 */
public interface IGetGeneration {
    /**
     * Gets a Collection of recent alive Cells for the GoLView to know where
     * these cells must be painted on the Component.
     * @return the Collection of recent alive cells.
     */
    Collection getCellCollection();
}
